# Good-Fast-Cheap (Julio's Practical Test)
![alt text](https://bitbucket.org/TheMaverink/good-fast-cheap/raw/3d4bbbb876c6b7ac1701db57d13e37800e11370e/src/assets/imgs/logo.png)

## Install and run locally

```bash
git clone git clone https://TheMaverink@bitbucket.org/TheMaverink/good-fast-cheap.git
```

> :warning: **Please make sure you use Node version 14 or above!**

Once cloned..

```bash
cd good-fast-cheap
```
```bash
npm install
```
```bash
npm run start
```
> :warning: **App is not responsive yet, please don't use on mobile**

## Code patterns and tech used
I tried to complete this challenge with the technologies and patterns that I usually work with.
Some of the components and patterns were a bit over engineered but i thought would be a good idea to present the project with the tools that i usually work with.

## General Rules:

- ✅ Solution should be a complete React app (Any manner of creation or bootstrapping is allowed)
- ✅ Solution should be emailed as a .zip or link to git repo (excluding node_modules)
- ✅ Include a screen capture .gif or video of it working (no audio needed)
- ✅ Use recent React and Node versions
- ✅ Recreate the “Good, Fast, Cheap” (as presented in gif)
- ✅ Use redux

##  Final Result:

![Alt Text](https://bitbucket.org/TheMaverink/good-fast-cheap/raw/bc95388c02aa15c12a2c22250a1bfbb3093e2ad7/src/assets/imgs/good-fast-cheap.gif)


Thanks!
If you have any questions or problems running the app locally please let me know.