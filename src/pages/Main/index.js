import React from 'react'
import styled from 'styled-components'
import { compose } from 'recompose'

import ToggleContainer from 'containers/Toggle'
import Toggle from 'components/Toggle'

import { ThumbsUp, PersonSimpleRun, Coin } from 'phosphor-react'

import { TOGGLES_ENUMS } from 'consts/toggles'

const { good, fast, cheap } = TOGGLES_ENUMS

const MainPageWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: hidden;

  .toggles-wrapper {
    width: 50%;
    height: 100%;
    padding: 160px 75px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }
`

const MainPage = (props) => {
  const { isLoading, errorMessage, currentActiveToggles, changeToggleRequest } =
    props

  return (
    <MainPageWrapper>
      <div className="toggles-wrapper">
        <Toggle
          name={good}
          icon={<ThumbsUp weight="fill" size={24} />}
          value={currentActiveToggles[good]}
          action={() => changeToggleRequest(good, !currentActiveToggles[good])}
        />
        <Toggle
          name={fast}
          icon={<PersonSimpleRun weight="fill" size={24} />}
          value={currentActiveToggles[fast]}
          action={() => changeToggleRequest(fast, !currentActiveToggles[fast])}
        />
        <Toggle
          name={cheap}
          icon={<Coin weight="fill" size={24} />}
          value={currentActiveToggles[cheap]}
          action={() =>
            changeToggleRequest(cheap, !currentActiveToggles[cheap])
          }
        />
      </div>
    </MainPageWrapper>
  )
}

export default compose(ToggleContainer)(MainPage)
