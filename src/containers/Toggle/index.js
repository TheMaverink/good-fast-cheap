import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';


import { changeToggleRequest } from './actions';

import {
  selectIsLoading,
  selectErrorMessage,
  selectCurrentActiveToggles,
  
} from './selectors';

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  errorMessage: selectErrorMessage,
  currentActiveToggles: selectCurrentActiveToggles,
});

export const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      changeToggleRequest,
    },
    dispatch
  );

export default compose(connect(mapStateToProps, mapDispatchToProps));
