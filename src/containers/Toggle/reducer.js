import {
  CHANGE_TOGGLE_REQUEST,
  CHANGE_TOGGLE_FAILURE,
  CHANGE_TOGGLE_SUCCESS,
} from './actions'

import { TOGGLES_ENUMS } from 'consts/toggles'

const { good, fast, cheap } = TOGGLES_ENUMS

const initialState = {
  loading: false,
  errorMessage: null,
  lastActivatedToggle: null,
  currentActiveToggles: {
    [good]: false,
    [fast]: false,
    [cheap]: false,
  },
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_TOGGLE_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case CHANGE_TOGGLE_FAILURE: {
      return {
        ...state,
        loading: false,
        currentActiveToggles: null,
        lastActivatedToggle: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case CHANGE_TOGGLE_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        lastActivatedToggle: action.payload.toggleValue
          ? action.payload.toggleName
          : state.lastActivatedToggle,
        currentActiveToggles: {
          ...state.currentActiveToggles,
          [action.payload.toggleName]: action.payload.toggleValue,
        },
      }
    }

    default:
      return state
  }
}
