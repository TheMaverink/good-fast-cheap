import { takeLatest, select, put } from 'redux-saga/effects'

import { REDUCER_NAME } from './consts'

import {
  CHANGE_TOGGLE_REQUEST,
  changeToggleFailure,
  changeToggleSuccess,
} from './actions'

function* changeToggleWorker(action) {
  try {
    const { toggleName, toggleValue } = action.payload

    const togglesState = yield select()

    const { currentActiveToggles, lastActivatedToggle } =
      yield togglesState[REDUCER_NAME]

    const numberOfActiveToggles = yield Object.entries(
      currentActiveToggles,
    ).filter((item) => {
      return item[0] !== toggleName && item[1]
    }).length

    if (numberOfActiveToggles === 2) {
      yield put(changeToggleSuccess({ toggleName, toggleValue }))

      yield put(
        changeToggleSuccess({
          toggleName: lastActivatedToggle,
          toggleValue: false,
        }),
      )
    } else {
      yield put(changeToggleSuccess({ toggleName, toggleValue }))
    }
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(changeToggleFailure(errorMessage))
  }
}

export default function* watcher() {
  yield takeLatest(CHANGE_TOGGLE_REQUEST, changeToggleWorker)
}
