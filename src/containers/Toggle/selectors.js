import { createSelector } from 'reselect'
import { REDUCER_NAME } from './consts'

export const selectIsLoading = createSelector(
  (state) => state[REDUCER_NAME],
  (reducer) => reducer['loading'],
)

export const selectErrorMessage = createSelector(
  (state) => state[REDUCER_NAME],
  (reducer) => reducer['errorMessage'],
)

export const selectCurrentActiveToggles = createSelector(
  (state) => state[REDUCER_NAME],
  (reducer) => reducer['currentActiveToggles'],
)
