export const CHANGE_TOGGLE_REQUEST = 'CHANGE_TOGGLE_REQUEST'
export const CHANGE_TOGGLE_FAILURE = 'CHANGE_TOGGLE_FAILURE'
export const CHANGE_TOGGLE_SUCCESS = 'CHANGE_TOGGLE_SUCCESS'

export const changeToggleRequest = (toggleName, toggleValue) => ({
  type: CHANGE_TOGGLE_REQUEST,
  payload: {
    toggleName,
    toggleValue,
  },
})

export const changeToggleFailure = (errorMessage) => ({
  type: CHANGE_TOGGLE_FAILURE,
  payload: errorMessage,
})

export const changeToggleSuccess = (newToggleState) => ({
  type: CHANGE_TOGGLE_SUCCESS,
  payload: newToggleState,
})
