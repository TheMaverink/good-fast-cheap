import styled from 'styled-components'

const ToggleWrapper = styled.div`
  /* TOGGLE */

  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;

  .toggle {
    cursor: pointer;
    overflow: hidden;
    position: relative;
    

    height: var(--toggle-height);
    width: var(--toggle-width);
    border-radius: calc(var(--toggle-height) * 2);
    white-space: nowrap;
    transition: 0.75s 0.1s linear;
    -webkit-tap-highlight-color: transparent;
    box-shadow: -0.4px 0px 0.5px -0.1px var(--bg-light), 0 0 0.5px 0.3px #b78729,
      0 0 0px 5px var(--toggle-border-color_disabled), 0 -0.3px 0px 5px #ffca60,
      0px 10px 11px 2px #00000078;

    &_enabled {
      box-shadow: 0.4px 0px 0.5px -0.1px var(--bg-light),
        0 0 0.5px 0.3px #b78729, 0 0 0px 5px var(--toggle-border-color_enabled),
        0 -0.3px 0px 5px #ffca60, 0px 10px 11px 2px #00000078;

      .toggle-circle {
        transform: translate(var(--toggle-height));
      }

      .toggle-bg_enabled,
      .toggle-bg_disabled {
        transform: translate(0);
      }
    }

    .toggle-bg {
      display: inline-block;
      height: var(--toggle-height);
      width: calc(var(--toggle-width) * 1.2);

      transform: translateX(calc(--toggle-height * -0.5));
      transition: 0.75s 0.1s linear;

      &_enabled {
        background-color: var(--toggle-bg-color_enabled);
        border-top-left-radius: 50px;
        border-bottom-left-radius: 50px;
        box-shadow: inset 5px 8px 10px -7px #4d4d4d82;
      }

      &_disabled {
        background-color: var(--toggle-bg-color_disabled);
        border-top-right-radius: 50px;
        border-bottom-right-radius: 50px;
        box-shadow: inset -5px 8px 10px -7px #4d4d4d82;
      }
    }

    .toggle-circle {
      height: var(--toggle-height);
      width: var(--toggle-height);
      overflow: hidden;
      position: absolute;
      top: 0;
      border-radius: 50%;
      white-space: nowrap;
      transition: 0.75s 0.1s linear;
      box-shadow: 0px 0px 20px 0px #4d4d4d82;

      &_enabled{
        transition: 0.75s 0.1s linear;
        background-color: var(--toggle-circle-color_enabled);
        box-shadow: inset 2px -4px 14px -11px var(--toggle-circle-color_enabled),
          inset -4px 11px 12px -10px #dcbebe;
      }

      &_disabled{
        transition: 0.75s 0.1s linear;
        background-color: var(--toggle-circle-color_disabled);
       box-shadow: inset -2px -4px 14px -11px #000,
          inset 4px 11px 12px -10px #f2804e;
      }
    }

    .toggle-circle-bg {
      position: relative;
      display: inline-block;
      width: ${(props) => props.height}px;
      height: ${(props) => props.height}px;
      transition: 0.75s 0.1s linear;

      &_enabled {
        background-color: var(--toggle-circle-color_enabled);
        transform: translateX(calc(--toggle-height * -0.5));
        box-shadow: inset 2px -4px 14px -11px #FFF,
          inset -4px 11px 12px -10px #FFF;
      }

      &_disabled {
        background-color: var(--toggle-circle-color_disabled);
        transform: translateX(calc(--toggle-height * -1));

        box-shadow: inset -2px -4px 14px -11px #000,
          inset 4px 11px 12px -10px #f2804e;
      }
    }
  }
  .toggle-info {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    transition: 0.75s 0.1s linear;
    min-width:120px;

    .toggle-label {
      color: white;
      font-family: RobotoMedium;
      transition: 0.75s 0.1s linear;
      position: relative;

      &_enabled {
        opacity: 1;
        font-size: 32px;
        color: var(--toggle-text_enabled);

        &:after {
          transition: 1s 0.1s linear;
          content: '';
          height: 0;
          width: 0;
        }
      }

      &_disabled {
        opacity: 0.75;
        font-size: 30px;
        color: var(--toggle-text_disabled);

        &:after {
          transition: 1s 0.1s linear;
          content: '';
          height: 2px;
          width: 100%;
          color: white;
          background-color: var(--toggle-text_disabled);
          position: absolute;
          top: 50%;
          left: 0;
        }
      }
    }

    &_enabled {
      svg {
        fill: var(--toggle-text_enabled);
        transition: 0.75s 0.1s linear;
        transform: scale(1.1);
      }
    }

    &_disabled {
      svg {
        fill: var(--toggle-text_disabled);
        transform: scale(0.9) rotate(180deg);
        transition: 0.75s 0.1s linear;
        opacity: 0;
      }
    }
  }
`

export default ToggleWrapper
