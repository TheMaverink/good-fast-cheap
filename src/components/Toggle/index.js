import React from 'react'
import ToggleWrapper from './ToggleWrapper'

const Toggle = (props) => {
  const { name, icon, value, action } = props

  const classModifier = React.useMemo(() => {
    return value ? `enabled` : `disabled`
  }, [value])

  return (
    <ToggleWrapper value={value} onClick={action}>
      <div className={`toggle toggle_${classModifier}`}>
        <div className={`toggle-bg toggle-bg_${classModifier}`}></div>

        <div className={`toggle-circle toggle-circle_${classModifier}`}>
          <div
            className={`toggle-circle-bg toggle-circle-bg_${classModifier}`}
          ></div>
        </div>
      </div>

      <div className={`toggle-info toggle-info_${classModifier}`}>
        <label className={`toggle-label toggle-label_${classModifier}`}>
          {name}
        </label>
        {icon}
      </div>
    </ToggleWrapper>
  )
}

export default Toggle
