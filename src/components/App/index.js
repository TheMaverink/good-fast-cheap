import React from 'react';
import styled from 'styled-components';

import GlobalStyles from './globalStyles';

const AppWrapper = styled.div``;

const App = ({ children }) => {
  return (
    <AppWrapper>
      <GlobalStyles />
      {children}
    </AppWrapper>
  );
};

export default App;
