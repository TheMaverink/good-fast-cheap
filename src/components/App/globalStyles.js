import { createGlobalStyle } from 'styled-components'

import RobotoMedium from 'assets/fonts/Roboto/Roboto-Medium.ttf'

import { BRAND_COLORS } from 'consts/brand'
import { TOGGLES_HEIGHT } from 'consts/toggles'

import Color from 'color'

const GlobalStyles = createGlobalStyle`

:root{

//COLOURS

--brand-blue: ${BRAND_COLORS.blue};

--brand-orange: ${BRAND_COLORS.orange};

--bg-light: ${Color(BRAND_COLORS.orange).lighten(0.25)};
--bg-med: ${Color(BRAND_COLORS.orange).lighten(0.1)};
--bg-dark: ${Color(BRAND_COLORS.orange).darken(0.05)};

//TOGGLES

--toggle-height: ${TOGGLES_HEIGHT}px;
--toggle-width: ${TOGGLES_HEIGHT * 2}px;

--toggle-border-color_enabled: ${Color(BRAND_COLORS.orange).lighten(0.3)};
--toggle-border-color_disabled: ${Color(BRAND_COLORS.orange).lighten(0.1)};

--toggle-circle-color_enabled: ${Color(BRAND_COLORS.blue).lighten(0.3)};
--toggle-circle-color_disabled: ${Color(BRAND_COLORS.orange).darken(0.14)};

--toggle-bg-color_enabled: ${Color(BRAND_COLORS.blue).lighten(0.16)};
--toggle-bg-color_disabled: ${Color(BRAND_COLORS.orange).darken(0.04)};

--toggle-text_enabled:${Color(BRAND_COLORS.blue).lighten(0.7)};
--toggle-text_disabled:${Color(BRAND_COLORS.orange).lighten(0.45)};


//FONTS

@font-face {
  font-family: 'RobotoMedium';
  src: local('RobotoMedium'), url(${RobotoMedium}) format('truetype');
}


}

* {
  box-sizing: border-box;
}

html,
body {
    margin: 0;
    padding:0;
    font-family:'RobotoRegular';
    background: var(--bg-med);
    background: linear-gradient(159deg, var(--bg-light)0%, var(--bg-dark) 100%);
  }

`

export default GlobalStyles
