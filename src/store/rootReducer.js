import { combineReducers } from 'redux';

import { REDUCER_NAME as toggleReducerName } from '../containers/Toggle/consts';
import toggleReducer from '../containers/Toggle/reducer';

const rootReducer = () =>
  combineReducers({
    [toggleReducerName]: toggleReducer,
  });

export default rootReducer;
