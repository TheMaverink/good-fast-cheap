import { all, fork } from 'redux-saga/effects';
import toggleSaga from '../containers/Toggle/sagas';

export default function* rootSaga() {
  yield all([fork(toggleSaga)]);
}
